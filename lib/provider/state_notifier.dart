import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:progress_state_button/progress_button.dart';

import 'package:flutter_attendance/models/punch_history_model.dart';
import 'package:flutter_attendance/models/punch_button_state_model.dart';
import 'package:flutter_attendance/models/punch_histories_list_model.dart';
import 'package:flutter_attendance/database/database.dart';

// 打刻ボタン状態
class PunchButtonStateNotifier extends StateNotifier<PunchButtonState> {
  AppDatabase database = AppDatabase();

  PunchButtonStateNotifier({required int punchState})
      : super(PunchButtonState(punchState: punchState));

  void setLoading() {
    state = state.copyWithLoading();
  }

  Future<void> pressedPunchInButton() async {
    if (state.punchButtonState == ButtonState.idle) {
      setLoading();
      DateTime now = DateTime.now();
      try {
        await database.insert(PunchHistory(
          year: now.year,
          month: now.month,
          day: now.day,
          hour: now.hour,
          minute: now.minute,
          punchType: 1,
        ));
        state = state.copyWithPressedPunchButton();
      } catch (e) {
        state = state.copyWithPunchInFailure();
      }
    } else if (state.punchButtonState == ButtonState.success) {
      state = PunchButtonState(punchState: 1);
    }
  }

  Future<void> pressedPunchOutButton() async {
    if (state.outButtonState == ButtonState.idle) {
      setLoading();
      DateTime now = DateTime.now();
      try {
        await database.insert(PunchHistory(
          year: now.year,
          month: now.month,
          day: now.day,
          hour: now.hour,
          minute: now.minute,
          punchType: 2,
        ));
        state = state.copyWithPressedOutButton();
      } catch (e) {
        state = state.copyWithPunchOutFailure();
      }
    } else if (state.outButtonState == ButtonState.success) {
      state = PunchButtonState(punchState: 2);
    }
  }

  Future<void> switchResting(bool changedIsResting) async {
    DateTime now = DateTime.now();
    await database.insert(PunchHistory(
      year: now.year,
      month: now.month,
      day: now.day,
      hour: now.hour,
      minute: now.minute,
      punchType: changedIsResting ? 4 : 5,
    ));
    state = state.copyWithChangedRestSwitch(changedIsResting);
  }

  Future<void> reset() async {
    await database.removeTodaysPunchHistories();
    state = state.copyWithReset();
  }
}

// 打刻履歴
class PunchHistoriesNotifier
    extends StateNotifier<AsyncValue<PunchHistoriesList>> {
  AppDatabase database = AppDatabase();

  PunchHistoriesNotifier() : super(const AsyncValue.loading()) {
    fetch();
  }

  Future<void> fetch() async {
    state = AsyncValue.data(await database.getPunchHistories());
  }

  Future<PunchHistoriesList> getItemsByDate(
      int year, int month, int day) async {
    return await database.getPunchHistoriesByDate(year, month, day);
  }

  void addItem(PunchHistory punchHistory) async {
    database.insert(punchHistory);
    fetch();
  }

  void updateItem(PunchHistory punchHistory) async {
    database.update(punchHistory);
    fetch();
  }

  void removeItem(int id) async {
    database.delete(id);
    fetch();
  }

  void removeAllItem() async {
    database.removeAllHistories();
    fetch();
  }
}
