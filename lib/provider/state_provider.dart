import 'package:flutter_riverpod/flutter_riverpod.dart';

import 'package:flutter_attendance/models/punch_button_state_model.dart';
import 'package:flutter_attendance/models/punch_histories_list_model.dart';
import 'package:flutter_attendance/provider/state_notifier.dart';

// 打刻
final punchButtonStateProvider = StateNotifierProvider.autoDispose
    .family<PunchButtonStateNotifier, PunchButtonState, int>(
        (ref, punchState) => PunchButtonStateNotifier(punchState: punchState));

// 履歴
final punchHistoriesProvider = StateNotifierProvider<PunchHistoriesNotifier,
    AsyncValue<PunchHistoriesList>>((ref) => PunchHistoriesNotifier());

final calenderSelectedDayProvider =
    StateProvider<DateTime>((ref) => DateTime.now());

final calenderFocusedDayProvider =
    StateProvider<DateTime>((ref) => DateTime.now());
