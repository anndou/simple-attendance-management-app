import 'package:flutter/material.dart';
import 'package:flutter_attendance/views/settings_page.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:intl/date_symbol_data_local.dart';

import 'package:flutter_attendance/views/punch_page.dart';
import 'package:flutter_attendance/views/calender.dart';

void main() {
  initializeDateFormatting();
  runApp(const ProviderScope(child: MyApp()));
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
          textTheme:
              GoogleFonts.zenMaruGothicTextTheme((Theme.of(context).textTheme)),
          primarySwatch: Colors.lightBlue),
      home: const MyHomePage(),
    );
  }
}

class MyHomePage extends StatefulWidget {
  const MyHomePage({Key? key}) : super(key: key);

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  int currentIndex = 0;

  static List<Widget> pageList = [
    PunchPage(),
    Calender(),
    SettingsPage(),
  ];
  static List<BottomNavigationBarItem> bottomNavigationBarItemList = [
    BottomNavigationBarItem(icon: Icon(Icons.punch_clock), label: '打刻'),
    BottomNavigationBarItem(icon: Icon(Icons.calendar_month), label: '履歴'),
    BottomNavigationBarItem(icon: Icon(Icons.settings), label: '設定'),
  ];

  Widget build(BuildContext context) {
    return Scaffold(
      body: pageList[currentIndex],
      bottomNavigationBar: BottomNavigationBar(
        items: bottomNavigationBarItemList,
        currentIndex: currentIndex,
        onTap: onTapped,
      ),
    );
  }

  void onTapped(int index) {
    setState(() {
      currentIndex = index;
    });
  }
}
