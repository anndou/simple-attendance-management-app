import 'package:path/path.dart';
import 'package:sqflite/sqflite.dart';
import 'package:flutter_attendance/models/punch_history_model.dart';
import 'package:flutter_attendance/models/punch_histories_list_model.dart';

class AppDatabase {
  static String fileName = 'attendance.db';

  late Database database;
  final String tableName = 'punch_histories';

  Future<Database> get getDatabase async {
    database = await initDB();
    return database;
  }

  Future<Database> initDB() async {
    String path = join(await getDatabasesPath(), fileName);
    return await openDatabase(path, version: 1, onCreate: createTable);
  }

  Future<void> createTable(Database db, int varsion) async {
    await db.execute('''
      CREATE TABLE punch_histories(
        id INTEGER PRIMARY KEY AUTOINCREMENT,
        year INTEGER,
        month INTEGER,
        day INTEGER,
        hour INTEGER,
        minute INTEGER,
        second INTEGER,
        punch_type INTEGER
      )
    ''');
    int id = 1;
    for (int i = 1; i < DateTime.now().day; i++) {
      insert(PunchHistory(
          id: id,
          year: 2022,
          month: 8,
          day: i,
          hour: 10,
          minute: 0,
          punchType: 1));
      id++;
      insert(PunchHistory(
          id: id,
          year: 2022,
          month: 8,
          day: i,
          hour: 13,
          minute: 0,
          punchType: 4));
      id++;
      insert(PunchHistory(
          id: id,
          year: 2022,
          month: 8,
          day: i,
          hour: 14,
          minute: 0,
          punchType: 5));
      id++;
      insert(PunchHistory(
          id: id,
          year: 2022,
          month: 8,
          day: i,
          hour: 19,
          minute: 0,
          punchType: 2));
      id++;
    }
  }

  Future<PunchHistoriesList> getPunchHistories() async {
    final db = await getDatabase;
    final maps = await db.query(
      tableName,
      orderBy: 'year ASC, month ASC, day ASC, hour ASC, minute ASC, second ASC',
    );

    if (maps.isEmpty) return PunchHistoriesList([]);

    return PunchHistoriesList(
        maps.map((e) => PunchHistory.fromJson(e)).toList());
  }

  Future<PunchHistoriesList> getPunchHistoriesByDate(
    int year,
    int month,
    int day,
  ) async {
    final db = await getDatabase;
    final maps = await db.query(
      tableName,
      where: 'year = ? AND month = ? AND day = ?',
      whereArgs: [year, month, day],
      orderBy: 'hour ASC, minute ASC, second ASC',
    );

    if (maps.isEmpty) return PunchHistoriesList([]);

    return PunchHistoriesList(
        maps.map((e) => PunchHistory.fromJson(e)).toList());
  }

  Future<PunchHistory> getTodaysLastPunchHistory() async {
    final db = await getDatabase;
    final DateTime today = DateTime.now();
    final maps = await db.query(
      tableName,
      where: 'year = ? AND month = ? AND day = ? AND hour < 24',
      whereArgs: [today.year, today.month, today.day],
      orderBy: 'hour DESC, minute DESC, second DESC',
    );

    if (maps.isEmpty) {
      return PunchHistory.emptyFirstData(today.year, today.month, today.day);
    }

    return maps.map((e) => PunchHistory.fromJson(e)).toList()[0];
  }

  Future<int> getTodaysLastPunchState() async {
    PunchHistory punchHistory = await getTodaysLastPunchHistory();
    return punchHistory.punchType;
  }

  Future<PunchHistory> insert(PunchHistory punchHistory) async {
    final db = await getDatabase;
    final id = await db.insert(
      tableName,
      punchHistory.toJson(),
      conflictAlgorithm: ConflictAlgorithm.replace,
    );

    return PunchHistory(
      id: id,
      year: punchHistory.year,
      month: punchHistory.month,
      day: punchHistory.day,
      hour: punchHistory.hour,
      minute: punchHistory.minute,
      punchType: punchHistory.punchType,
    );
  }

  Future update(PunchHistory punchHistory) async {
    final db = await getDatabase;

    await db.update(
      tableName,
      punchHistory.toJson(),
      where: 'id = ?',
      whereArgs: [punchHistory.id],
      conflictAlgorithm: ConflictAlgorithm.replace,
    );
  }

  Future delete(int id) async {
    final db = await getDatabase;

    await db.delete(
      tableName,
      where: 'id = ?',
      whereArgs: [id],
    );
  }

  Future removeAllHistories() async {
    final db = await getDatabase;

    await db.delete(
      tableName,
    );
  }

  Future removeTodaysPunchHistories() async {
    final DateTime today = DateTime.now();
    final db = await getDatabase;

    await db.delete(
      tableName,
      where: 'year = ? AND month = ? AND day = ?',
      whereArgs: [today.year, today.month, today.day],
    );
  }
}
