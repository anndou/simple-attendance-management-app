import 'dart:collection';

import 'package:flutter_attendance/models/punch_history_model.dart';

class PunchHistoriesList extends ListBase {
  final List<PunchHistory> punchHistoriesList;

  PunchHistoriesList(this.punchHistoriesList);

  @override
  int get length => punchHistoriesList.length;
  @override
  set length(int newLength) => punchHistoriesList.length = newLength;
  @override
  PunchHistory operator [](int index) => punchHistoriesList[index];
  @override
  void operator []=(int index, dynamic elem) =>
      punchHistoriesList[index] = elem;

  List<PunchHistory> filterDate(int year, int month, int day) {
    return punchHistoriesList
        .where((PunchHistory h) =>
            (h.year == year && h.month == month && h.day == day))
        .toList();
  }

  PunchHistoriesList today() {
    DateTime today = DateTime.now();
    List<PunchHistory> todaysPunchHistories =
        filterDate(today.year, today.month, today.day);
    if (todaysPunchHistories.isEmpty) return PunchHistoriesList([]);
    return PunchHistoriesList(todaysPunchHistories);
  }

  PunchHistoriesList day(int year, int month, int day) {
    List<PunchHistory> todaysPunchHistories = filterDate(year, month, day);
    if (todaysPunchHistories.isEmpty) return PunchHistoriesList([]);
    return PunchHistoriesList(todaysPunchHistories);
  }

  int getTodaysLastPunchState() {
    DateTime today = DateTime.now();
    List<PunchHistory> todaysList =
        filterDate(today.year, today.month, today.day);

    if (todaysList.isEmpty) return 0;
    return todaysList.last.punchType;
  }

  bool hasPunchInHistory() {
    return hasPunchTypeHistory(1);
  }

  bool hasPunchOutHistory() {
    return hasPunchTypeHistory(2);
  }

  bool hasRestInHistory() {
    return hasPunchTypeHistory(4);
  }

  bool hasRestOutHistory() {
    return hasPunchTypeHistory(5);
  }

  bool hasPunchTypeHistory(int punchType) {
    if (punchHistoriesList.isEmpty) return false;
    List<PunchHistory> filteredList = punchHistoriesList
        .where((PunchHistory h) => (h.punchType == punchType))
        .toList();
    return !filteredList.isEmpty;
  }

  String getWorkingTime() {
    if (punchHistoriesList.isEmpty) return "";
    int workingMinutes = calcWorkingMinutes();
    // ex) 7.5 h
    return (workingMinutes / 60).toStringAsFixed(1) + "h";
  }

  int calcWorkingMinutes() {
    DateTime punchTime = getTime(0, 0);
    DateTime outTime = getTime(DateTime.now().hour, DateTime.now().minute);
    DateTime? prevRestInTime;
    int restMinutes = 0;
    punchHistoriesList.forEach((PunchHistory e) {
      if (e.punchType == 1) {
        punchTime = getTime(e.hour, e.minute);
      }
      if (e.punchType == 2) {
        outTime = getTime(e.hour, e.minute);
      }
      if (e.punchType == 4) {
        prevRestInTime = getTime(e.hour, e.minute);
      }
      if (e.punchType == 5 && prevRestInTime != null) {
        restMinutes +=
            getTime(e.hour, e.minute).difference(prevRestInTime!).inMinutes;
        prevRestInTime = null;
      }
    });
    if (outTime == getTime(0, 0)) return 0;
    return outTime.difference(punchTime).inMinutes - restMinutes;
  }

  DateTime getTime(hour, minute) {
    return DateTime(1999, 1, 1, hour, minute, 0, 0, 0);
  }

  String toJson() {
    String txt = '[';
    punchHistoriesList.forEach((element) {
      txt += '{';
      txt += '\"id\":\"' + element.id.toString() + '\",';
      txt += '\"year\":\"' + element.year.toString() + '\",';
      txt += '\"month\":\"' + element.month.toString() + '\",';
      txt += '\"day\":\"' + element.day.toString() + '\",';
      txt += '\"hour\":\"' + element.hour.toString() + '\",';
      txt += '\"minute\":\"' + element.minute.toString() + '\",';
      txt += '\"second\":\"' + element.second.toString() + '\",';
      txt += '\"punchType\":\"' + element.punchType.toString() + '\",';
      txt += '},';
    });
    txt += ']';
    return txt;
  }
}
