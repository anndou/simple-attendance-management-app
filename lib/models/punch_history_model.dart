import 'package:flutter_attendance/models/punch_type_model.dart';

class PunchHistory {
  late int? id;
  late int year;
  late int month;
  late int day;
  late int hour;
  late int minute;
  late int second;
  late int punchType;
  late bool canEdit;

  PunchHistory({
    this.id,
    required this.year,
    required this.month,
    required this.day,
    required this.hour,
    required this.minute,
    required this.punchType,
    this.canEdit = true,
  }) {
    setSecond();
  }

  setSecond() {
    second = DateTime.now().second;
  }

  PunchHistory.emptyFirstData(
    int year,
    int month,
    int day,
  ) {
    year = year;
    month = month;
    day = day;
    hour = 0;
    minute = 0;
    punchType = 3;
    canEdit = false;
    setSecond();
  }

  PunchHistory.emptyEndData(
    int year,
    int month,
    int day,
  ) {
    year = year;
    month = month;
    day = day;
    hour = 24;
    minute = 0;
    punchType = 3;
    canEdit = false;
    setSecond();
  }

  PunchHistory.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    year = json['year'];
    month = json['month'];
    day = json['day'];
    hour = json['hour'];
    minute = json['minute'];
    punchType = json['punch_type'];
    canEdit = true;
    setSecond();
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = id;
    data['year'] = year;
    data['month'] = month;
    data['day'] = day;
    data['hour'] = hour;
    data['minute'] = minute;
    data['second'] = second;
    data['punch_type'] = punchType;
    return data;
  }

  String get workingStatus => PunchType[punchType];

  bool get isWorking => (punchType != 3);

  bool get isRest => (punchType == 4);

  bool get isEndWorking => (punchType == 2);

  String get time =>
      hour.toString().padLeft(2, '0') + ":" + minute.toString().padLeft(2, '0');

  String get date =>
      year.toString() + "年" + month.toString() + "月" + day.toString() + "日";
}
