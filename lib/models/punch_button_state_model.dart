import 'package:flutter/material.dart';
import 'package:progress_state_button/progress_button.dart';

@immutable
class PunchButtonState {
  late int punchType;
  late IconData mainIcon;
  late String attendStatus;
  late String message;
  late ButtonState punchButtonState;
  late ButtonState outButtonState;
  late bool isResting;
  late String restSwitchText;
  late bool visiblePunchButton;
  late bool visibleOutButton;
  late bool visibleRestSwitch;

  PunchButtonState({int punchState = 0}) {
    punchType = punchState;
    mainIcon = Icons.computer;
    attendStatus = '出勤前';
    message = '';
    punchButtonState = ButtonState.idle;
    outButtonState = ButtonState.idle;
    isResting = false;
    restSwitchText = '休憩する';
    visiblePunchButton = true;
    visibleOutButton = false;
    visibleRestSwitch = false;
    switch (punchType) {
      case 1:
      case 5:
        // 勤務中（出勤後・休憩終了後）
        visiblePunchButton = false;
        visibleOutButton = true;
        visibleRestSwitch = true;
        attendStatus = '勤務中';
        message = '';
        break;
      case 4:
        // 休憩中
        visiblePunchButton = false;
        visibleOutButton = true;
        visibleRestSwitch = true;
        attendStatus = '勤務中';
        message = '';
        isResting = true;
        restSwitchText = '休憩から戻る';
        break;
      case 2:
        // 勤務終了
        visiblePunchButton = false;
        attendStatus = '勤務終了';
        message = 'お疲れさまでした！';
        mainIcon = Icons.single_bed;
        break;
    }
  }

  PunchButtonState copyWithPressedPunchButton() {
    PunchButtonState newPunchButtonState =
        PunchButtonState(punchState: punchType);
    switch (punchButtonState) {
      case ButtonState.idle:
      case ButtonState.loading:
        newPunchButtonState.punchButtonState = ButtonState.success;
        newPunchButtonState.message = '';
        newPunchButtonState.attendStatus = '勤務中';
        newPunchButtonState.mainIcon = Icons.computer;
        break;
      case ButtonState.success:
        newPunchButtonState.punchButtonState = ButtonState.idle;
        newPunchButtonState.message = '';
        if (visiblePunchButton) {
          newPunchButtonState.visiblePunchButton = false;
          newPunchButtonState.visibleOutButton = true;
        } else {
          newPunchButtonState.visiblePunchButton = true;
          newPunchButtonState.visibleOutButton = false;
        }
        break;
      case ButtonState.fail:
        newPunchButtonState.punchButtonState = ButtonState.idle;
        newPunchButtonState.message = '';
        break;
    }
    return newPunchButtonState;
  }

  PunchButtonState copyWithPressedOutButton() {
    PunchButtonState newPunchButtonState =
        PunchButtonState(punchState: punchType);

    switch (outButtonState) {
      case ButtonState.idle:
      case ButtonState.loading:
        newPunchButtonState.outButtonState = ButtonState.success;
        newPunchButtonState.message = 'お疲れさまでした！';
        newPunchButtonState.attendStatus = '勤務終了';
        newPunchButtonState.mainIcon = Icons.single_bed;
        newPunchButtonState.visibleRestSwitch = false;
        break;
      case ButtonState.success:
        newPunchButtonState.outButtonState = ButtonState.idle;
        newPunchButtonState.message = '';
        newPunchButtonState.attendStatus = '出勤前';
        newPunchButtonState.mainIcon = Icons.rice_bowl;

        if (visiblePunchButton) {
          newPunchButtonState.visiblePunchButton = false;
          newPunchButtonState.visibleOutButton = true;
        } else {
          newPunchButtonState.visiblePunchButton = true;
          newPunchButtonState.visibleOutButton = false;
        }
        break;
      case ButtonState.fail:
        newPunchButtonState.outButtonState = ButtonState.idle;
        newPunchButtonState.message = '';
        break;
    }
    return newPunchButtonState;
  }

  PunchButtonState copyWithPunchInFailure() {
    PunchButtonState newPunchButtonState =
        PunchButtonState(punchState: punchType);

    newPunchButtonState.punchButtonState = ButtonState.fail;
    newPunchButtonState.message = 'もう一度ボタンを押してください。';
    return newPunchButtonState;
  }

  PunchButtonState copyWithPunchOutFailure() {
    PunchButtonState newPunchButtonState =
        PunchButtonState(punchState: punchType);

    newPunchButtonState.outButtonState = ButtonState.fail;
    newPunchButtonState.message = 'もう一度ボタンを押してください。';
    return newPunchButtonState;
  }

  PunchButtonState copyWithChangedRestSwitch(bool changedIsResting) {
    PunchButtonState newPunchButtonState =
        PunchButtonState(punchState: punchType);

    if (changedIsResting) {
      newPunchButtonState.isResting = true;
      newPunchButtonState.restSwitchText = '休憩から戻る';
    } else {
      newPunchButtonState.isResting = false;
      newPunchButtonState.restSwitchText = '休憩に入る';
    }

    return newPunchButtonState;
  }

  PunchButtonState copyWithLoading() {
    PunchButtonState newPunchButtonState =
        PunchButtonState(punchState: punchType);
    if (visiblePunchButton) {
      newPunchButtonState.punchButtonState = ButtonState.loading;
    } else if (visibleOutButton) {
      newPunchButtonState.outButtonState = ButtonState.loading;
    }
    return newPunchButtonState;
  }

  PunchButtonState copyWithReset() {
    PunchButtonState newPunchButtonState = PunchButtonState(punchState: 0);
    newPunchButtonState.message = '打刻データをリセットしました。';
    return newPunchButtonState;
  }
}
