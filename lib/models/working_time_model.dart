class WorkingTimeModel {
  late int hour;
  late int minute;

  WorkingTimeModel({
    required this.hour,
    required this.minute,
  });

  String get time =>
      hour.toString().padLeft(2, '0') + ":" + minute.toString().padLeft(2, '0');
}
