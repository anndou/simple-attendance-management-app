import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_attendance/models/punch_histories_list_model.dart';
import 'package:flutter_attendance/provider/state_provider.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:settings_ui/settings_ui.dart';

class SettingsPage extends ConsumerStatefulWidget {
  @override
  SettingsPageState createState() => SettingsPageState();
}

class SettingsPageState extends ConsumerState<SettingsPage> {
  @override
  Widget build(BuildContext context) {
    double _height = MediaQuery.of(context).size.height;
    double _width = MediaQuery.of(context).size.width;

    return Container(
        child: SettingsList(
      sections: [
        CustomSettingsSection(
            child: Container(
          height: _height * 0.1,
        )),
        SettingsSection(title: Text('その他'), tiles: <SettingsTile>[
          SettingsTile.navigation(
              leading: Icon(Icons.copy),
              title: Text('JSONデータをコピー'),
              onPressed: (BuildContext context) async {
                AsyncValue<PunchHistoriesList> list =
                    await ref.watch(punchHistoriesProvider);
                list.when(
                  data: (data) {
                    Clipboard.setData(ClipboardData(text: data.toJson()));
                  },
                  error: (error, stackTrace) => Text(""),
                  loading: () => Text(""),
                );
                showDialog(
                    context: context,
                    builder: (BuildContext context) => AlertDialog(
                          title: const Text(
                            'クリップボードにコピーしました。',
                            style: TextStyle(fontSize: 16),
                          ),
                          actions: <Widget>[
                            TextButton(
                              onPressed: (() {
                                Navigator.pop(context);
                              }),
                              child: Text('OK'),
                            ),
                          ],
                        ));
              })
        ]),
        SettingsSection(title: Text('リセット'), tiles: <SettingsTile>[
          SettingsTile.navigation(
              leading: Icon(Icons.delete_forever),
              title: Text('打刻データを削除'),
              onPressed: (BuildContext context) async {
                showDialog(
                    context: context,
                    builder: (BuildContext context) => AlertDialog(
                          title: const Text(
                            'すべてのデータを削除します。',
                            style: TextStyle(fontSize: 16),
                          ),
                          content: const Text(
                            'この操作はもとに戻せません。よろしいですか？',
                            style: TextStyle(fontSize: 16),
                          ),
                          actions: <Widget>[
                            TextButton(
                              onPressed: (() => Navigator.pop(context)),
                              child: Text('キャンセル'),
                            ),
                            TextButton(
                              onPressed: (() {
                                ref
                                    .read(punchHistoriesProvider.notifier)
                                    .removeAllItem();
                                Navigator.pop(context);
                              }),
                              child: Text('削除'),
                            ),
                          ],
                        ));
              })
        ]),
      ],
    ));
  }
}
