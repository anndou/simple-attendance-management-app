import 'package:flutter/material.dart';
import 'package:percent_indicator/percent_indicator.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:table_calendar/table_calendar.dart';
import 'package:flutter_attendance/provider/state_provider.dart';
import 'package:flutter_attendance/models/punch_histories_list_model.dart';
import 'package:flutter_attendance/views/components/punch_history_view.dart';

class Calender extends ConsumerWidget {
  late AsyncValue<PunchHistoriesList> punchHistories;
  List getEventsForDay(DateTime date) {
    return punchHistories.when(
        loading: () => [],
        error: ((error, stackTrace) => []),
        data: (data) => data.day(date.year, date.month, date.day));
  }

  Widget eventMakerBuilder(
      BuildContext context, DateTime day, punchHistoriesList) {
    return Consumer(builder: ((context, ref, child) {
      double _height = MediaQuery.of(context).size.height;
      double _width = MediaQuery.of(context).size.width;

      int percent = 0;
      if (punchHistoriesList.hasPunchInHistory()) percent += 40;
      if (punchHistoriesList.hasRestInHistory()) percent += 10;
      if (punchHistoriesList.hasRestOutHistory()) percent += 10;
      if (punchHistoriesList.hasPunchOutHistory()) percent += 40;

      bool isWhiteTextColor = (isSameDay(DateTime.now(), day) ||
          isSameDay(ref.watch(calenderFocusedDayProvider), day));

      return Container(
        padding: EdgeInsets.only(top: _height * 0.03),
        child: CircularPercentIndicator(
          radius: _height * 0.011,
          lineWidth: _height * 0.011,
          percent: percent / 100,
          animation: true,
          footer: Text(
            punchHistoriesList.getWorkingTime(),
            style: TextStyle(
              fontSize: _height * 0.011,
              color: isWhiteTextColor ? Colors.white : Colors.black87,
            ),
          ),
          center: Visibility(
            child: Icon(
              Icons.check,
              color: Colors.white,
              size: _height * 0.014,
            ),
            visible: percent == 100,
          ),
          progressColor:
              percent == 100 ? Colors.green.shade400 : Colors.lightBlue,
        ),
      );
    }));
  }

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    double _height = MediaQuery.of(context).size.height;
    double _width = MediaQuery.of(context).size.width;
    punchHistories = ref.watch(punchHistoriesProvider);
    final nowSelectedDay = ref.watch(calenderSelectedDayProvider);
    final nowFocusedDay = ref.watch(calenderFocusedDayProvider);

    DateTime now = DateTime.now();
    DateTime firstDay = now.add(Duration(days: -360));
    DateTime lastDay = now.add(Duration(days: 90));

    return Container(
      height: _height * 0.7,
      padding: EdgeInsets.only(top: _height * 0.1),
      child: TableCalendar(
        locale: 'ja_JP',
        shouldFillViewport: true,
        firstDay: DateTime(firstDay.year, firstDay.month, 1),
        lastDay: DateTime(lastDay.year, lastDay.month, 1),
        focusedDay: nowFocusedDay,
        headerStyle: HeaderStyle(
          titleCentered: true,
          formatButtonVisible: false,
        ),
        calendarStyle: CalendarStyle(
          markersAlignment: Alignment.center,
          cellAlignment: Alignment.topCenter,
          todayDecoration: BoxDecoration(
            color: const Color(0xFF9FA8DA),
            shape: BoxShape.rectangle,
            borderRadius: BorderRadius.circular(_height * 0.01),
          ),
          selectedDecoration: BoxDecoration(
            color: const Color(0xFF5C6BC0),
            shape: BoxShape.rectangle,
            borderRadius: BorderRadius.circular(_height * 0.01),
          ),
          defaultDecoration: BoxDecoration(
            shape: BoxShape.rectangle,
            borderRadius: BorderRadius.circular(_height * 0.01),
          ),
          weekendDecoration: BoxDecoration(
            shape: BoxShape.rectangle,
            borderRadius: BorderRadius.circular(_height * 0.01),
          ),
        ),
        calendarBuilders: CalendarBuilders(
          markerBuilder: eventMakerBuilder,
        ),
        selectedDayPredicate: (day) => isSameDay(nowSelectedDay, day),
        eventLoader: getEventsForDay,
        onDaySelected: ((selectedDay, focusedDay) {
          showModalBottomSheet(
              isScrollControlled: true,
              context: context,
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.vertical(
                top: Radius.circular(15),
              )),
              builder: (BuildContext context) => Container(
                    height: _height * 0.85,
                    child: punchHistories.when(
                      data: (data) => PunchHistoryView(
                        year: selectedDay.year,
                        month: selectedDay.month,
                        day: selectedDay.day,
                        punchHistoriesData: data.day(selectedDay.year,
                            selectedDay.month, selectedDay.day),
                      ),
                      error: ((error, stackTrace) =>
                          Text('エラーが発生しました。 ${error.toString()}')),
                      loading: () =>
                          const Center(child: CircularProgressIndicator()),
                    ),
                  ));
          if (!isSameDay(nowSelectedDay, selectedDay)) {
            ref.read(calenderSelectedDayProvider.notifier).state = selectedDay;
            ref.read(calenderFocusedDayProvider.notifier).state = focusedDay;
          }
        }),
      ),
    );
  }
}
