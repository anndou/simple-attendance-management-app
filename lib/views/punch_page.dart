import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:flutter_attendance/provider/state_provider.dart';
import 'package:flutter_attendance/views/components/punchin_button.dart';
import 'package:flutter_attendance/views/components/punchout_button.dart';
import 'package:flutter_attendance/views/components/punchrest_switch.dart';
import 'package:flutter_attendance/models/punch_button_state_model.dart';
import 'package:flutter_attendance/models/punch_histories_list_model.dart';

class PunchPage extends ConsumerWidget {
  late AsyncValue<PunchHistoriesList> punchHistories;
  late PunchButtonState punchButtonState;

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    double _height = MediaQuery.of(context).size.height;
    double _width = MediaQuery.of(context).size.width;
    final AsyncValue<PunchHistoriesList> punchHistories =
        ref.watch(punchHistoriesProvider);

    return punchHistories.when(
        loading: () => Center(child: const CircularProgressIndicator()),
        error: ((error, stackTrace) =>
            Center(child: Text('エラーが発生しました。 ${error.toString()}'))),
        data: (punchHistoriesList) {
          PunchButtonState punchButtonState = ref.watch(
              punchButtonStateProvider(
                  punchHistoriesList.getTodaysLastPunchState()));
          return Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Icon(
                punchButtonState.mainIcon,
                size: _height * 0.1,
              ),
              Container(
                height: _height * 0.03,
              ),
              Text(punchButtonState.attendStatus,
                  style: TextStyle(fontSize: _height * 0.04)),
              Container(
                height: _height * 0.03,
              ),
              Visibility(
                child:
                    PunchInButton(lastPunchState: punchButtonState.punchType),
                visible: punchButtonState.visiblePunchButton,
              ),
              Visibility(
                child:
                    PunchOutButton(lastPunchState: punchButtonState.punchType),
                visible: punchButtonState.visibleOutButton,
              ),
              Container(
                height: _height * 0.03,
              ),
              Padding(
                padding:
                    EdgeInsets.only(left: _width * 0.04, right: _width * 0.05),
                child: Visibility(
                  child: PunchRestSwitch(
                      lastPunchState: punchButtonState.punchType),
                  visible: punchButtonState.visibleRestSwitch,
                ),
              ),
              Container(
                height: _height * 0.03,
              ),
              Text(punchButtonState.message),
            ],
          );
        });
  }
}
