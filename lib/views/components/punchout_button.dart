import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:flutter_attendance/provider/state_provider.dart';
import 'package:flutter_attendance/models/punch_button_state_model.dart';
import 'package:flutter_attendance/views/components/punch_button.dart';

class PunchOutButton extends ConsumerStatefulWidget {
  late int lastPunchState;

  PunchOutButton({Key? key, required this.lastPunchState}) : super(key: key);
  @override
  PunchOutButtonState createState() =>
      PunchOutButtonState(lastPunchState: lastPunchState);
}

class PunchOutButtonState extends ConsumerState<PunchOutButton> {
  late int lastPunchState;
  late PunchButtonState punchButtonState;

  PunchOutButtonState({Key? key, required this.lastPunchState});

  @override
  Widget build(BuildContext context) {
    punchButtonState = ref.watch(punchButtonStateProvider(lastPunchState));
    return PunchButton(
      text: "退勤",
      icon: Icons.mode_night,
      backgroundColor: Colors.deepPurple.shade300,
      onPressed: () async {
        await ref
            .read(punchButtonStateProvider(punchButtonState.punchType).notifier)
            .pressedPunchOutButton();
        await ref.read(punchHistoriesProvider.notifier).fetch();
      },
    );
  }
}
