import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:flutter_attendance/provider/state_provider.dart';
import 'package:flutter_attendance/models/punch_button_state_model.dart';
import 'package:flutter_attendance/views/components/punch_button.dart';

class PunchInButton extends ConsumerStatefulWidget {
  late int lastPunchState;

  PunchInButton({Key? key, required this.lastPunchState}) : super(key: key);
  @override
  PunchInButtonState createState() =>
      PunchInButtonState(lastPunchState: lastPunchState);
}

class PunchInButtonState extends ConsumerState<PunchInButton> {
  late int lastPunchState;
  late PunchButtonState punchButtonState;

  PunchInButtonState({Key? key, required this.lastPunchState});

  @override
  Widget build(BuildContext context) {
    punchButtonState = ref.watch(punchButtonStateProvider(lastPunchState));
    return PunchButton(
      text: "出勤",
      icon: Icons.sunny,
      backgroundColor: Colors.blue.shade400,
      onPressed: () async {
        await ref
            .read(punchButtonStateProvider(punchButtonState.punchType).notifier)
            .pressedPunchInButton();
        await ref.read(punchHistoriesProvider.notifier).fetch();
      },
    );
  }
}
