import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:flutter_attendance/provider/state_provider.dart';

class PunchResetButton extends ConsumerWidget {
  late int lastPunchState;
  PunchResetButton({Key? key, required this.lastPunchState}) : super(key: key);

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    return ElevatedButton(
        child: Text(" * 今日の打刻データをリセットする"),
        style: ElevatedButton.styleFrom(
          primary: Colors.red.shade200,
          onPrimary: Colors.white,
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(10),
          ),
        ),
        onPressed: () async {
          await ref
              .read(punchButtonStateProvider(lastPunchState).notifier)
              .reset();
          await ref.read(punchHistoriesProvider.notifier).fetch();
        });
  }
}
