import 'package:flutter/material.dart';
import 'package:flutter_attendance/models/punch_history_model.dart';
import 'package:flutter_attendance/models/punch_histories_list_model.dart';
import 'package:flutter_attendance/views/components/punch_history_timeline.dart';

class PunchHistoryView extends StatelessWidget {
  late PunchHistoriesList punchHistories;
  final int year;
  final int month;
  final int day;
  PunchHistoryView({
    Key? key,
    required this.year,
    required this.month,
    required this.day,
    required punchHistoriesData,
  }) : super(key: key) {
    punchHistories = PunchHistoriesList([
      PunchHistory.emptyFirstData(
        year,
        month,
        day,
      ),
      ...punchHistoriesData,
      PunchHistory.emptyEndData(
        year,
        month,
        day,
      ),
    ]);
  }

  @override
  Widget build(BuildContext context) {
    double _height = MediaQuery.of(context).size.height;
    double _width = MediaQuery.of(context).size.width;
    return Center(
        child: Container(
            child: Card(
                margin: EdgeInsets.all(_width * 0.06),
                child: Column(mainAxisSize: MainAxisSize.min, children: [
                  Padding(
                      padding: EdgeInsets.all(_width * 0.06),
                      child: Text(
                          year.toString() +
                              "年" +
                              month.toString() +
                              "月" +
                              day.toString() +
                              "日",
                          style: TextStyle(fontSize: _height * 0.024))),
                  Divider(
                      indent: _width * 0.06,
                      endIndent: _width * 0.06,
                      color: Colors.grey),
                  SizedBox(
                    height: _height * 0.6,
                    child: Padding(
                      padding: EdgeInsets.only(
                          left: _width * 0.1, right: _width * 0.1),
                      child: PunchHistoryTimeLine(punchHistories),
                    ),
                  )
                ]))));
  }
}
