import 'package:flutter/material.dart';
import 'package:flutter_attendance/provider/state_provider.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:timelines/timelines.dart';

import 'package:flutter_attendance/models/punch_histories_list_model.dart';
import 'package:flutter_attendance/views/components/punch_history_timeline_label.dart';

class PunchHistoryTimeLine extends ConsumerWidget {
  PunchHistoriesList punchHistories;
  PunchHistoryTimeLine(this.punchHistories);

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    ref.watch(punchHistoriesProvider);
    double _height = MediaQuery.of(context).size.height;
    double _width = MediaQuery.of(context).size.width;
    return Timeline.tileBuilder(
      theme: TimelineThemeData(
        nodePosition: 0,
        color: Colors.grey.shade400,
        connectorTheme: ConnectorThemeData(thickness: _width * 0.006),
      ),
      builder: TimelineTileBuilder.connected(
          indicatorBuilder: (context, index) {
            if (punchHistories[index].isEndWorking) {
              return Icon(
                Icons.check_circle,
                color: Colors.green,
              );
            }
            if (punchHistories[index].isWorking) {
              return Icon(
                Icons.circle,
                color: punchHistories[index].isWorking
                    ? Colors.blue.shade400
                    : null,
              );
            }
            return Icon(
              Icons.circle_outlined,
              color: Colors.grey.shade400,
            );
          },
          connectorBuilder: (_, index, connectorType) {
            var color;
            if (punchHistories[index].isRest) {
              color = Colors.lightBlue.shade100;
            } else if (punchHistories[index].isWorking &&
                punchHistories[index + 1].isWorking) {
              color = Colors.lightBlue.shade500;
            } else {
              color = null;
            }
            return SolidLineConnector(
              indent: connectorType == ConnectorType.start ? 0 : _width * 0.005,
              endIndent:
                  connectorType == ConnectorType.end ? 0 : _width * 0.005,
              color: color,
            );
          },
          contentsBuilder: (_, index) => Padding(
                padding: EdgeInsets.only(left: _width * 0.05),
                child: PunchHistoryTimeLineLabel(punchHistories[index]),
              ),
          itemExtentBuilder: (_, __) => _height * 0.1,
          itemCount: punchHistories.length),
    );
  }
}
