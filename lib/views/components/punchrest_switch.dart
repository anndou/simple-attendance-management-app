import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:flutter_attendance/models/punch_button_state_model.dart';
import 'package:flutter_attendance/provider/state_provider.dart';

class PunchRestSwitch extends ConsumerStatefulWidget {
  late int lastPunchState;
  PunchRestSwitch({Key? key, required this.lastPunchState}) : super(key: key);

  @override
  PunchRestSwitchState createState() =>
      PunchRestSwitchState(lastPunchState: lastPunchState);
}

class PunchRestSwitchState extends ConsumerState<PunchRestSwitch> {
  late int lastPunchState;
  late PunchButtonState punchButtonState;

  PunchRestSwitchState({Key? key, required this.lastPunchState});

  @override
  Widget build(BuildContext context) {
    double _width = MediaQuery.of(context).size.width;
    punchButtonState = ref.watch(punchButtonStateProvider(lastPunchState));
    return Container(
        width: _width * 0.7,
        child: SwitchListTile(
          title: Text(punchButtonState.restSwitchText),
          value: punchButtonState.isResting,
          onChanged: (bool changedIsResting) async {
            await ref
                .read(punchButtonStateProvider(punchButtonState.punchType)
                    .notifier)
                .switchResting(changedIsResting);
            await ref.read(punchHistoriesProvider.notifier).fetch();
          },
          secondary: const Icon(Icons.scuba_diving),
        ));
  }
}
