import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

class PunchButton extends ConsumerWidget {
  late String text;
  late IconData icon;
  late Color backgroundColor;
  late void Function() onPressed;

  PunchButton({
    Key? key,
    required String this.text,
    required IconData this.icon,
    required Color this.backgroundColor,
    required void Function() this.onPressed,
  }) : super(key: key);

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    return ElevatedButton.icon(
        label: Text(
          text,
          style: TextStyle(
            fontSize: 16,
            color: Colors.white,
          ),
        ),
        icon: Icon(icon, color: Colors.white),
        style: TextButton.styleFrom(
          enableFeedback: true,
          backgroundColor: backgroundColor,
          padding: EdgeInsets.only(top: 10, bottom: 10, right: 25, left: 25),
          shape: const RoundedRectangleBorder(
            borderRadius: BorderRadius.all(Radius.circular(100)),
          ),
        ),
        onPressed: onPressed);
  }
}
