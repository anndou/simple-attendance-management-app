import 'package:flutter/material.dart';
import 'package:flutter_attendance/provider/state_provider.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:flutter_picker/flutter_picker.dart';

import 'package:flutter_attendance/models/punch_history_model.dart';

class PunchHistoryTimeLineLabel extends ConsumerWidget {
  PunchHistory punchHistory;
  PunchHistoryTimeLineLabel(this.punchHistory);

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    ref.watch(punchHistoriesProvider);
    double _height = MediaQuery.of(context).size.height;
    double _width = MediaQuery.of(context).size.width;

    if (punchHistory.canEdit) {
      return Row(children: [
        Text(
          punchHistory.time,
          style: TextStyle(fontSize: _height * 0.023),
        ),
        GestureDetector(
            child: Icon(
              Icons.mode_edit_outlined,
              size: _height * 0.017,
              color: Colors.grey.shade600,
            ),
            onTap: () {
              Picker(
                  adapter: DateTimePickerAdapter(
                    customColumnType: [3, 4],
                    value: DateTime(
                      punchHistory.year,
                      punchHistory.month,
                      punchHistory.day,
                      punchHistory.hour,
                      punchHistory.minute,
                    ),
                  ),
                  onConfirm: (picker, selected) {
                    punchHistory.hour = selected[0];
                    punchHistory.minute = selected[1];
                    ref
                        .read(punchHistoriesProvider.notifier)
                        .updateItem(punchHistory);
                  }).showModal(context);
            }),
        Text("　"),
        Text(
          punchHistory.workingStatus,
          style: TextStyle(fontSize: _height * 0.023, height: 2),
        ),
        GestureDetector(
            child: Container(
              alignment: Alignment.centerRight,
              child: Icon(
                Icons.delete_outline_rounded,
                size: _height * 0.019,
                color: Colors.red.shade300,
              ),
            ),
            onTap: () async {
              showDialog(
                  context: context,
                  builder: (BuildContext context) => AlertDialog(
                        title: const Text('削除してよろしいですか？'),
                        actions: <Widget>[
                          TextButton(
                            onPressed: (() => Navigator.pop(context)),
                            child: Text('キャンセル'),
                          ),
                          TextButton(
                            onPressed: (() {
                              ref
                                  .read(punchHistoriesProvider.notifier)
                                  .removeItem(punchHistory.id!);
                              Navigator.pop(context);
                              Navigator.pop(context);
                            }),
                            child: Text('削除'),
                          ),
                        ],
                      ));
            }),
      ]);
    }
    return Text(punchHistory.time + "　" + punchHistory.workingStatus,
        style: TextStyle(fontSize: _height * 0.023));
  }
}
